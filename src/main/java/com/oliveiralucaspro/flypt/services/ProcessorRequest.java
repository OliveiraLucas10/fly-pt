package com.oliveiralucaspro.flypt.services;

import com.oliveiralucaspro.flypt.dto.ExternalRequestDTO;
import com.oliveiralucaspro.flypt.dto.FlightDetailDTO;
import com.oliveiralucaspro.flypt.exceptions.OutboundServiceException;

import java.util.List;

public interface ProcessorRequest {
	
	List<FlightDetailDTO> getFlightDetailResponse(ExternalRequestDTO externalRequestDTO) throws OutboundServiceException ;

}
