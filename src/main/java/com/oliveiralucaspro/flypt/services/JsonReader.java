package com.oliveiralucaspro.flypt.services;

import com.oliveiralucaspro.flypt.dto.ExternalRequestDTO;
import com.oliveiralucaspro.flypt.dto.FlightDTO;
import com.oliveiralucaspro.flypt.enums.Destination;
import com.oliveiralucaspro.flypt.exceptions.OutboundServiceException;

import java.util.List;

public interface JsonReader {

	List<FlightDTO> getFlightsFromJson(Destination destination, ExternalRequestDTO externalRequestDTO) throws OutboundServiceException;

}
