package com.oliveiralucaspro.flypt.services;

import com.oliveiralucaspro.flypt.dto.FlightDTO;

import java.math.BigDecimal;
import java.util.List;

public interface FlightsAnalyzer {

	BigDecimal getPricesAverage(List<FlightDTO> flights);

	BigDecimal getBagOnePricesAverage(List<FlightDTO> flights);

	BigDecimal getBagTwoPricesAverage(List<FlightDTO> flights);

}
