package com.oliveiralucaspro.flypt.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FlightResponseDTO {
	@JsonValue
	List<FlightDetailDTO> flights;

}
