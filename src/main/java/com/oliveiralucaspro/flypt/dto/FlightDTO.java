package com.oliveiralucaspro.flypt.dto;

import com.google.gson.annotations.SerializedName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FlightDTO {

	private String flyTo;
	private BigDecimal price;

	@SerializedName("bags_price")
	private BagInfoDTO bag;

	public BagInfoDTO getBag() {
		return this.bag == null ? new BagInfoDTO() : this.bag;
	}
}
