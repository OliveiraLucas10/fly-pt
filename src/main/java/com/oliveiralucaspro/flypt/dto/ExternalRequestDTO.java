package com.oliveiralucaspro.flypt.dto;

import com.oliveiralucaspro.flypt.enums.Destination;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ExternalRequestDTO {

	private List<Destination> destinations;
	private LocalDate dateFrom;
	private LocalDate dateTo;

}
