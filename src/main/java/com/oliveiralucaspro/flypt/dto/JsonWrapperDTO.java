package com.oliveiralucaspro.flypt.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JsonWrapperDTO<T> {

	private List<T> data;

}
